<?php
/**
 * @file
 * Definition of Drupal\breakpoint\Tests\BreakpointMediaQueryTest.
 */

namespace Drupal\breakpoint\Tests;

use Drupal\simpletest\UnitTestBase;
use Drupal\breakpoint\Breakpoint;
use Exception;

/**
 * Tests for media queries in a breakpoint.
 */
class BreakpointMediaQueryTest extends UnitTestBase {

  /**
   * Drupal\simpletest\WebTestBase\getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => 'Breakpoint media query tests',
      'description' => 'Test validation of media queries.',
      'group' => 'Breakpoint',
    );
  }

  /**
   * Test valid media queries.
   */
  public function testValidMediaQueries() {
    $media_queries = array(
      // Bartik breakpoints.
      '(min-width: 0px)',
      'all and (min-width: 560px) and (max-width:850px)',
      'all and (min-width: 851px)',
      // Seven breakpoints.
      '(min-width: 0em)',
      'screen and (min-width: 40em)',
      // Stark breakpoints.
      '(min-width: 0px)',
      'all and (min-width: 480px) and (max-width: 959px)',
      'all and (min-width: 960px)',
      '(orientation)',
      'all and (orientation)',
      'not all and (orientation)',
      'only all and (orientation)',
      'screen and (width)',
      'screen and (width: 0)',
      'screen and (width: 0px)',
      'screen and (width: 0em)',
      'screen and (min-width: -0)',
      'screen and (max-width: 0)',
      'screen and (min-width)',
      // Multiline and comments.
      'screen and /* this is a comment */ (min-width)',
      "screen\nand /* this is a comment */ (min-width)",
      "screen\n\nand /* this is\n a comment */ (min-width)",
    );

    foreach ($media_queries as $media_query) {
      try {
        $this->assertTrue(Breakpoint::isValidMediaQuery($media_query), $media_query . ' is valid.');
      }
      catch (Exception $e) {
        $this->assertTrue(FALSE, $media_query . ' is valid.');
      }
    }
  }

  /**
   * Test invalid media queries.
   */
  public function testInvalidMediaQueries() {
    $media_queries = array(
      'not (orientation)',
      'only (orientation)',
      'all and not all',
      'screen and (width: 0xx)',
      'screen and (width: -8xx)',
      'screen and (width: -xx)',
      'screen and (width: xx)',
      'screen and (width: px)',
      'screen and (width: -8px)',
      'screen and (width: -0.8px)',
      'screen and (height: 0xx)',
      'screen and (height: -8xx)',
      'screen and (height: -xx)',
      'screen and (height: xx)',
      'screen and (height: px)',
      'screen and (height: -8px)',
      'screen and (height: -0.8px)',
      'screen and (device-width: 0xx)',
      'screen and (device-width: -8xx)',
      'screen and (device-width: -xx)',
      'screen and (device-width: xx)',
      'screen and (device-width: px)',
      'screen and (device-width: -8px)',
      'screen and (device-width: -0.8px)',
      'screen and (device-height: 0xx)',
      'screen and (device-height: -8xx)',
      'screen and (device-height: -xx)',
      'screen and (device-height: xx)',
      'screen and (device-height: px)',
      'screen and (device-height: -8px)',
      'screen and (device-height: -0.8px)',
      'screen and (min-orientation)',
      'screen and (max-orientation)',
      'screen and (min-orientation: landscape)',
      'screen and (max-orientation: landscape)',
      'screen and (orientation: bogus)',
      '(orientation: bogus)',
    );

    foreach ($media_queries as $media_query) {
      try {
        $this->assertFalse(Breakpoint::isValidMediaQuery($media_query), $media_query . ' is not valid.');
      }
      catch (Exception $e) {
        $this->assertTrue(TRUE, $media_query . ' is not valid.');
      }
    }
  }
}
